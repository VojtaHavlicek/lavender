/*
 * =====================================================================================
 *
 *       Filename:  numerics.cpp
 *
 *    Description:  Numerical algorithms for lavender
 *
 *        Version:  1.0
 *        Created:  07/28/2013 08:34:01 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "numerics.h"
/*
 *--------------------------------------------------------------------------------------
 *       Class:  Numerics
 *      Method:  Numerics
 * Description:  constructor
 *--------------------------------------------------------------------------------------
 */
Numerics::Numerics ()
{
}  /* -----  end of method Numerics::Numerics  (constructor)  */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  euler_derivative
 *  Description:  Step increment using RK4 integration. Directly from 
 *
 *  		  http://gafferongames.com/game-physics/integration-basics/ 
 *
 *  		  as I did not come up with a better and more flexible way of 
 *  		  implementation than Geffer did. Thanks!
 * =====================================================================================
 */
Derivative Numerics::euler_derivative(const State &initial, float t, float dt, const Derivative &deriv)
{
	State state_;
	state_.position = initial.position + dt*deriv.d_position;
	state_.velocity = initial.velocity + dt*deriv.d_velocity;

	Derivative output_;
	output_.d_position = state_.velocity;
	output_.d_velocity = acceleration(state_, t+dt);

	return output_;
}



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  acceleration
 *  Description:  dummy acceleration function
 * =====================================================================================
 */
Vec2D Numerics::acceleration(const State &init, float t)
{
 	const float k = 10;
	const float b = 1;

	return (-k)*init.position + (-b)*init.velocity;
}



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  RK4
 *  Description:  integrates state using Runge-Kutta 4
 * =====================================================================================
 */
void Numerics::RK4(const State &state, float t, float dt)
{
	Derivative a_ = euler_derivative(state, t, 0.0f, Derivative());
	Derivative b_ = euler_derivative(state, t, 0.5*dt, a_);
	Derivative c_ = euler_derivative(state, t, 0.5*dt, b_);
	Derivative d_ = euler_derivative(state, t, dt, c_);

	Derivative d_state_;
	d_state_.d_position = (1.0f/6.0f)*(a_.d_position + (2.0f)*(b_.d_position + c_.d_position) + d_.d_position);
	d_state_.d_velocity = (1.0f/6.0f)*(a_.d_velocity + (2.0f)*(b_.d_velocity + c_.d_velocity) + d_.d_velocity);

	state.position = state.position + dt*d_state_.d_position;
	state.velocity = state.velocity + dt*d_state_.d_velocity;

} 













