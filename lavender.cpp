/*
 * =====================================================================================
 *
 *       Filename:  lavender.cpp
 *
 *    Description:  wrapper for cicada to set up openGL for canvas apps
 *
 *        Version:  1.0
 *        Created:  07/13/2013 11:13:15 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   Organization:  is not an anarchy 
 *
 * =====================================================================================
 */
#include "lavender.h"

/* 
 * ===  CONSTRUCTOR  ===================================================================
 *         Name:  lavender
 *  Description:  
 * =====================================================================================
 */
lavender::lavender()
{
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  init
 *  Description:  overrides the original init function in cicada with one setting up
 *                the OpenGL environment.
 * =====================================================================================
 */
void lavender::init(void (*update)(void))
{
    cicada::init(update, lavender::redraw, lavender::openGL_init);
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  openGL_init
 *  Description:  sets up openGL canvas
 * =====================================================================================
 */
void lavender::openGL_init()
{
   glScalef(1.0f, -1.0f, -1.0f); // flip y coord
   gluOrtho2D(0, 550, 0, 400);   // set up 2D orthographic projection 
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  redraw
 *  Description:  redraws the canvas based on the display list content
 * =====================================================================================
 */
void lavender::redraw()
{
    Sprite* sprite_ = Sprite::first;
   
    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_QUADS);
    
    for(int i = 0; i < 10 && sprite_ != NULL; i++) // Guard
    {
	sprite_->draw();
    }

    glEnd();
}

