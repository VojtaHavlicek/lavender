/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Cicada library example. Test program. TODO: make lavender extend 
 *    		    cicada directly.
 *
 *        Version:  1.0
 *        Created:  09/07/13 17:52:45
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "../lavender.h"
#include <stdlib.h>

Sprite* s;
lavender* l;
float x;
float v = 400;
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  update
 *  Description:  
 * =====================================================================================
 */
void update()
{
	x += l->get_time_elapsed()*v;
	s->setPos(10 + x, 10);

}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    s = new Sprite();     
    s->setPos(10,10);             
    s->reg();                     
    
    x = 0;

    l = new lavender();
    l->init(update); // starts the loop
    
    return EXIT_SUCCESS;
} /* ----------  end of function main  ---------- */















