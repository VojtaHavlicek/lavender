OPT=-O2 -lglfw3 -lGL -lGLU -lX11 -lpthread -lXxf86vm -lm -lrt -lXrandr -lXi -std=c++11 # compiler C flags

# LAVENDER TEST
LAVENDER_TEST=main

# Builds the project with lavender
lavender: numerics/numerics.cpp test/lavender.cpp display/sprite.cpp lavender.cpp 
	g++ numerics/numerics.cpp display/sprite.cpp lavender.cpp test/lavender.cpp -o $(LAVENDER_TEST) -lcicada $(OPT)
