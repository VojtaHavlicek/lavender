
/*-----------------------------------------------------------------------------
 *  data_structures.h
 *
 *  specification of data structures used in lavender
 *-----------------------------------------------------------------------------*/

struct Vec2D
{
    float x;
    float y;
};


/*-----------------------------------------------------------------------------
 *  a bit dirty way of operator implementation
 *-----------------------------------------------------------------------------*/

Vec2D operator+(const Vec2D &pos1, const Vec2D &pos2)
{
    Vec2D result;
    result.x = pos1->x + pos2->x;
    result.y = pos1->y + pox2->y;

    return result;
}

Vec2D operator*(float dt, const Vec2D &pos1)
{
    Vec2D result;
    result.x = pos1.x*dt;
    result.y = pos1.y*dt;

    return result;
}


struct State
{
    Vec2D position;
    Vec2D velocity;
};

struct Derivative
{
    Vec2D d_position;
    Vec2D d_velocity;
};
