/*
 * =====================================================================================
 *
 *       Filename:  data_structures.cpp
 *
 *    Description:  Definition of structures
 *
 *        Version:  1.0
 *        Created:  07/28/2013 12:59:01 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "data_structures.h"
/*-----------------------------------------------------------------------------
 *  a bit dirty way of operator implementation
 *-----------------------------------------------------------------------------*/

Vec2D operator+(const Vec2D &pos1, const Vec2D &pos2)
{
    Vec2D result;
    result.x = pos1.x + pos2.x;
    result.y = pos1.y + pos2.y;

    return result;
}

Vec2D operator*(float dt, const Vec2D &pos1)
{
    Vec2D result;
    result.x = pos1.x*dt;
    result.y = pos1.y*dt;

    return result;
}

