
/*-----------------------------------------------------------------------------
 *  lavender.h
 *
 *  INCLUDES:
 *-----------------------------------------------------------------------------*/
#pragma once
#include "display/sprite.h"
#include <cicada.h>

class lavender: public cicada
{
    public:
	/*-----------------------------------------------------------------------------
	 *  CONSTRUCTOR
	 *-----------------------------------------------------------------------------*/
	lavender(); // default construtor

	/*-----------------------------------------------------------------------------
	 *  FUNCTIONS
	 *-----------------------------------------------------------------------------*/
	void init(void (*update)(void)); 

	static void redraw();
	static void openGL_init(); // initializes openGL to pixel-to-pixel canvas
};



