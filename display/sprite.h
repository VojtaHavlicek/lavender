
/*-----------------------------------------------------------------------------
 *  sprite.h
 *-----------------------------------------------------------------------------*/
#pragma once
#include <cicada.h>
/*
 * =====================================================================================
 *        Class:  Sprite
 *  Description:  a simple rectangular sprite to be added on to display stack. 
 * =====================================================================================
 */
class Sprite
{
    public:
	/* ====================  LIFECYCLE     ======================================= */
	Sprite ();                             /* constructor */
	Sprite (float width, float height);

	/* ====================  ACCESSORS     ======================================= */

	/* ====================  MUTATORS      ======================================= */
	void setX(float value);
	void setY(float value);
	void setPos(float x, float y);

	/* ====================  OPERATORS     ======================================= */
	void draw();
	Sprite* next();       // returns the next sibling (for iteration);

	/* ====================  DATA MEMBERS  ======================================= */
	Sprite* next_sibling; // allows to access siblings in the display doubly-linked list
	Sprite* prev_sibling; 
	
	static Sprite* first;
	static Sprite* last;

	void reg(); // register to display list

    protected:
	/* ====================  METHODS       ======================================= */

	/* ====================  DATA MEMBERS  ======================================= */
	float x, y;
	float width, height;
    
    private:
	/* ====================  METHODS       ======================================= */

	/* ====================  DATA MEMBERS  ======================================= */

}; /* -----  end of class Sprite  ----- */



