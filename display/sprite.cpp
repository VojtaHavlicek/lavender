/*
 * =====================================================================================
 *
 *       Filename:  sprite.cpp
 *
 *    Description:  simple sprite container
 *
 *        Version:  1.0
 *        Created:  14/07/13 16:53:25
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "sprite.h"

/*
 *--------------------------------------------------------------------------------------
 *       Class:  Sprite
 *      Method:  Sprite
 * Description:  constructor - constructs a default 20x20 sprite registered on left-hand
 * 		 corner. The default paint color is white
 *--------------------------------------------------------------------------------------
 */
Sprite::Sprite ()
{
	width  = 20;
	height = 20;

	x = 0;
	y = 0;

}  /* -----  end of method Sprite::Sprite  (constructor)  ----- */

Sprite::Sprite(float width, float height)
{
	this->width = width;
	this->height = height;

	x = 0;
	y = 0;

}

/*-----------------------------------------------------------------------------
 *  STAT VARS
 *-----------------------------------------------------------------------------*/
Sprite* Sprite::first = NULL;
Sprite* Sprite::last = NULL;

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  draw
 *  Description:  draws the sprite
 * =====================================================================================
 */
void Sprite::draw()
{
	glColor3f(1,1,1);
	glVertex2f(x,y);
	glVertex2f(x + width, y);
	glVertex2f(x + width, y + height);
	glVertex2f(x, y + height);
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  add_to_display_list
 *  Description:  adds the object into the display list of the sprites
 * =====================================================================================
 */
void Sprite::reg()
{
    if(Sprite::first == NULL){
	Sprite::first = this;
	Sprite::last  = this;
    }else
    {   // make this to be the last element in the display list
	Sprite::last->next_sibling = this;
	this->prev_sibling = Sprite::last;
	Sprite::last = this;
    }
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  next
 *  Description: returns a reference to a next sibling 
 * =====================================================================================
 */
Sprite* Sprite::next()
{
    return next_sibling;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  setX
 *  Description:  sets the x position of the sprite
 * =====================================================================================
 */
void Sprite::setX(float value)
{
    this->x = value;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  setY
 *  Description:  sets the y-position of the sprite
 * =====================================================================================
 */

void Sprite::setY(float value)
{
    this->y = value;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  setPosition
 *  Description:  sets the x/y coord of the object
 * =====================================================================================
 */
void Sprite::setPos(float x, float y)
{
    this->x = x;
    this->y = y;
}


